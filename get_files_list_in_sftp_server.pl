use strict;
use warnings;
use Net::SFTP::Foreign;
use 5.010;

## README
## Perl 5.014 needed.
## Module Net::SFTP::Foreign needed. To install it via console:
## > cpan -i  Net::SFTP::Foreign

## To execute the script:
## > perl get_files_list_in_sftp_server.pl
## It connects to a SFTP server, makes ls in the server
## and retrieves thata list of files in STDOUT

######################
##### PARAMETERS #####
######################

my $host="sftp.tnsglobal.es";
my $port="22";
my $user="bitext_voc";
my $password="VoC15BiText7";
my $old_dir = "old";
my $old_dir_perms = '666';
my $down_suffix = "down";

my $DASH_re = '-';
my $VOC_re = 'VOC';
my $BT_re = 'BT';
my $files_re = qr/^$VOC_re$DASH_re$BT_re.*\.txt$/;

################
##### MAIN #####
################

# CREATE A NEW SFTP CONNECTION

my $sftp = Net::SFTP::Foreign->new(
    port => $port, host => $host, user => $user, password => $password);
$sftp->die_on_error("Unable to establish SFTP connection");

# Get all relevant files

my $remote_files = $sftp->ls(wanted => qr/$files_re/);
say for map { $_->{filename} } @$remote_files;

