use strict;
use warnings;
use Net::SFTP::Foreign;
use 5.010;

## README
## Perl 5.014 needed.
## Module Net::SFTP::Foreign needed. To install it via console:
## > cpan -i  Net::SFTP::Foreign

## To execute the script:
## > perl put_file_is_sftp_server.pl name_of_the_file

# It connects to a SFTP server and uploads a specific
# file taken as argument.

######################
##### PARAMETERS #####
######################

my $host="sftp.tnsglobal.es";
my $port="22";
my $user="bitext_voc";
my $password="VoC15BiText7";
my $old_dir = "old";
my $old_dir_perms = '666';
my $down_suffix = "down";

my $DASH_re = '-';
my $VOC_re = 'VOC';
my $BT_re = 'BT';
my $files_re = qr/^$VOC_re$DASH_re$BT_re.*\.txt$/;

################
##### MAIN #####
################

# CREATE A NEW SFTP CONNECTION

my $sftp = Net::SFTP::Foreign->new(
    port => $port, host => $host, user => $user, password => $password);
$sftp->die_on_error("Unable to establish SFTP connection");


my $file_name = shift @ARGV // 
    die "No file name provided";

# Prepared filesystem and get needed file names
# These functions must be here, at the beginning of the
# iscript cause they check that the names are righte

create_old_dir() unless $sftp->find($old_dir);
my $down_file = get_down_file_name($file_name); 
my $old_file_path = get_old_file_path($file_name);

# Upload the file

$sftp->put($file_name,$file_name) //
    die "Unable to upload file $file_name"; 

# Move in server the original downloaded file to ./old
$sftp->rename($down_file,$old_file_path) //
    die "Unable move $down_file to $old_file_path in server";

# Functions

sub create_old_dir
{
    my $old_dir_attrs = Net::SFTP::Foreign::Attributes->new();
    $old_dir_attrs->set_perm($old_dir_perms);
    $sftp->mkdir($old_dir,$old_dir_attrs);
}

sub get_original_file_name
{
    my $new_name = shift;

    my $FIRST = $BT_re.$DASH_re.$VOC_re;
    my ($REST) = $new_name =~ /$FIRST(.+)\.csv/;
    $REST // die "Unexpected name for file $file_name. It should be BT-VOC... ";  
    my $old_name = $VOC_re.$DASH_re.$BT_re.$REST.".txt";
}

sub get_down_file_name
{
    my $new_name = shift;

    my $old_name = get_original_file_name($new_name);
    my $down_name = $old_name.".".$down_suffix;
}

sub get_old_file_path
{
    my $new_name = shift;
    my $old_file_name = get_original_file_name($new_name);
    my $path = "./$old_dir/$old_file_name";
}
