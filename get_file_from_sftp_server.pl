use strict;
use warnings;
use Net::SFTP::Foreign;
use 5.010;

## README
## Perl 5.014 needed.
## Module Net::SFTP::Foreign needed. To install it via console:
## > cpan -i  Net::SFTP::Foreign

## To execute the script:
## > perl get_file_from_sftp_server.pl name_of_the_file

my $file_name = shift @ARGV // 
    die "No file name provided";

# It connects to a SFTP server and downloads a specific
# file taken as argument.

######################
##### PARAMETERS #####
######################

my $host="sftp.tnsglobal.es";
my $port="22";
my $user="bitext_voc";
my $password="VoC15BiText7";
my $old_dir = "old";
my $old_dir_perms = '666';
my $down_suffix = "down";

my $DASH_re = '-';
my $VOC_re = 'VOC';
my $BT_re = 'BT';
my $files_re = qr/^$VOC_re$DASH_re$BT_re.*\.txt$/;

################
##### MAIN #####
################

# CREATE A NEW SFTP CONNECTION

my $sftp = Net::SFTP::Foreign->new(
    port => $port, host => $host, user => $user, password => $password);
$sftp->die_on_error("Unable to establish SFTP connection");

$sftp->get($file_name) // 
    die "No file $file_name found in server";
my $down_file_name = get_down_file_name($file_name);
$sftp->rename($file_name,$down_file_name) // 
    die "Unable to rename $file_name as $down_file_name ";

sub get_down_file_name
{
    my $old_name = shift;
    my $new_name = $old_name.".".$down_suffix;
}
